# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

from scrapy import Item, Field
from scrapy.loader import ItemLoader
from itemloaders.processors import TakeFirst, Identity


class BookItem(Item):
    book_title = Field()
    book_price = Field()
    book_image_URL = Field()
    book_detail_page_URL = Field()


class BookItemLoader(ItemLoader):
    default_item_class = BookItem
    default_output_processor = TakeFirst()


class QuoteItem(Item):
    quote = Field()
    author = Field()
    tags = Field()


class QuoteItemLoader(ItemLoader):
    default_item_class = QuoteItem
    default_output_processor = TakeFirst()
    tags_out = Identity()
