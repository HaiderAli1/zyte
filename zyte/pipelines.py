# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
import logging


class CustomPipeline:
    """
    This pipeline will raise a warning if any field of the received item is null.
    """

    def process_item(self, item, spider):
        if spider.name == 'books_toscrape':
            if not item.get('book_title'):
                logging.warning("book_title is missing")

            if not item.get('book_price'):
                logging.warning("book_price is missing")

            if not item.get('book_image_URL'):
                logging.warning("book_image_URL is missing")

            if not item.get('book_detail_page_URL'):
                logging.warning("book_detail_page_URL is missing")

        elif spider.name == 'quotes_toscrape':
            if not item.get('quote'):
                logging.warning("quote is missing")

            if not item.get('author'):
                logging.warning("author is missing")

            if not item.get('tags'):
                logging.warning("tags are missing")

        return item
