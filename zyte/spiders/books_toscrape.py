import scrapy
from zyte.items import BookItemLoader


class BooksToscrapeSpider(scrapy.Spider):
    name = 'books_toscrape'
    allowed_domains = ['books.toscrape.com']
    start_urls = ['https://books.toscrape.com/']

    def parse(self, response):
        books = response.xpath('//section/div/ol[contains(@class , "row")]//li')
        for book in books:
            book_il = BookItemLoader(selector=book)
            book_il.add_xpath('book_title', './/h3/a/@title')
            book_il.add_xpath('book_price', './/p[@class="price_color"]/text()')
            book_img_url_rel = book.xpath('.//div[@class="image_container"]/a/img/@src').extract_first()
            book_il.add_value('book_image_URL', response.urljoin(book_img_url_rel))
            detail_page_url_rel = book.xpath('.//h3/a/@href').extract_first()
            book_il.add_value('book_detail_page_URL', response.urljoin(detail_page_url_rel))
            yield book_il.load_item()

        next_page = response.xpath('//li[@class="next"]/a/@href').extract_first()
        if next_page:
            yield response.follow(next_page, callback=self.parse)
