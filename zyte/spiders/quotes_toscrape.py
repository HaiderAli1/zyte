import scrapy
from scrapy_splash import SplashRequest
from zyte.items import QuoteItemLoader


class QuotesToscrapeSpider(scrapy.Spider):
    name = 'quotes_toscrape'
    allowed_domains = ['quotes.toscrape.com']

    custom_settings = {'SPLASH_URL': 'http://localhost:8050',
                       'DOWNLOADER_MIDDLEWARES': {
                           'scrapy_splash.SplashCookiesMiddleware': 723,
                           'scrapy_splash.SplashMiddleware': 725,
                           'scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware': 810,
                       },
                       'SPIDER_MIDDLEWARES': {
                           'scrapy_splash.SplashDeduplicateArgsMiddleware': 100,
                       },
                       'DUPEFILTER_CLASS': 'scrapy_splash.SplashAwareDupeFilter'
    }

    def start_requests(self):
        yield SplashRequest('https://quotes.toscrape.com/')

    def parse(self, response):
        quotes = response.xpath('//div[@class="quote"]')
        for quote in quotes:
            quote_il = QuoteItemLoader(selector=quote)
            quote_il.add_xpath('author', './/small[@itemprop="author"]/text()')
            quote_il.add_xpath('quote', './/span[@itemprop="text"]/text()')
            quote_il.add_xpath('tags', './/div[@class="tags"]//a/text()')
            yield quote_il.load_item()

        next_page = response.xpath('//ul/li[@class="next"]/a/@href').extract_first()
        if next_page:
            yield SplashRequest(response.urljoin(next_page))
